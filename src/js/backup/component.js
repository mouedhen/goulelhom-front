import Vue from 'vue';

import Heckd from './Component/Heckd';

new Vue({
  el: '#capp',
  render: h => h(Heckd)
});

const app = new Vue({
  components: {
    'app': Heckd
  },
}).$mount('#capp');
