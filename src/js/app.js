// import '@babel/polyfill'
import axios from 'axios'
import Vue from 'vue'
import 'vue2-dropzone/dist/vue2Dropzone.css';

window.Vue = Vue;
window.axios = axios;

window.axios.defaults.headers.common = {
  'X-Requested-With': 'XMLHttpRequest',
  'ACCESS-CONTROL-ALLOW-ORIGIN': '*',
};

import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import Vuex from 'vuex'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n);
Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(Vuex);

import router from './routes'

import ZStyle from './components/shared/ZStyle'
import ZSidebar from './components/shared/ZSidebar'
import App from './components/App.vue'


const i18n = new VueI18n({
    locale: process.env.DEFAULT_LOCALE, // set locale
});

import VeeValidate from 'vee-validate';

Vue.use(VeeValidate);

/*
const appStyle = new Vue({
    components: {
        'z-style': ZStyle
    },
    i18n
}).$mount('#styles');
*/

new Vue({
  el: '#z-sidebar',
  router,
  i18n,
  render: h => h(ZSidebar)
});

new Vue({
  el: '#app',
  router,
  i18n,
  render: h => h(App)
});


