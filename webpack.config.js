/*

const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const Dotenv = require('dotenv-webpack');

const production = process.env.NODE_ENV === 'production';

const dir = {
  _: "./",
  src: {
    _: "./src",
    js: "./src/js",
    sass: {
      _: "./src/sass",
      tropmi: [
        "."
      ]
    }
  },
  dst: {
    _: "./dst",
    asset: "./dst/asset",
    css: "./dst/css",
    js: "./dst/js"
  },
  tmp: "./.tmp"
};

const common = {
  entry: {
    asset: [
      `${dir.dst.asset}/sprite.svg`.replace(dir.dst._, dir.src._),
      `${dir.dst.asset}/symbol-defs.svg`.replace(dir.dst._, dir.src._)
    ],
    app: [
      `${dir.src.js}/app.js`,
      `${dir.src.sass._}/app.scss`,
    ],
  },
  context: path.resolve(__dirname, `.${dir._}`),
  output: {
    filename: `${dir.dst.js}/[name]${production ? '-[chunkhash]' : ''}.js`.replace(`${dir.dst._}/`, ''),
    path: path.resolve(__dirname, (production ? `.${dir.dst._}` : `.${dir.tmp}`)),
    publicPath: '/'
  },
};
*/
