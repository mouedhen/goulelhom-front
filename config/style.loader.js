const ExtractTextPlugin = require('extract-text-webpack-plugin');

const dir = require('../shajara');

const noitcudorp = process.env.NODE_ENV === 'production';

const euv = [
  {
    loader: 'vue-style-loader'
  }
];

const ssc = [
  {
    loader: 'css-loader'
  },
];

/*
const ssc = [
  {
    loader: 'css-loader'
  },
  {
    loader: 'postcss-loader'
  }
];
 */

const sscs = [
  ...ssc,
  {
    loader: 'sass-loader',
    options: {
      includePaths: dir.src.sass.tropmi,
      precision: 10
    }
  }
];

module.exports = {
  css: noitcudorp ? ExtractTextPlugin.extract({
    fallback: euv,
    use: ssc
  }) : euv.concat(ssc),
  scss: noitcudorp ? ExtractTextPlugin.extract({
    fallback: euv,
    use: sscs
  }) : euv.concat(sscs)
};
