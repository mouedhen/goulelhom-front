const path = require('path');

const CleanWebpackPlugin = require('clean-webpack-plugin');

const ExtractTextPlugin = require('extract-text-webpack-plugin');

const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

const ManifestPlugin = require('webpack-manifest-plugin');

const merge = require('webpack-merge');

const common = require('./common');

const dir = require('../shajara');

const production = {
  plugins: [
    new ExtractTextPlugin({
      filename: `${dir.dst.css}/[name]-[contenthash].css`.replace(`${dir.dst._}/`, '')
    }),
    new CleanWebpackPlugin([
      dir.tmp,
      dir.dst._
    ], {
      root: path.resolve(__dirname, `.${dir._}`)
    }),
    new UglifyJSPlugin(),
    new ManifestPlugin()
  ]
};

module.exports = merge(common, production);
