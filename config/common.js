const path = require('path');

const Dotenv = require('dotenv-webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackRTLPlugin = require('webpack-rtl-plugin');

const style = require('./style.loader');
const dir = require('../shajara');

const production = process.env.NODE_ENV === 'production';

const common = {
  entry: {
    asset: [
      `${dir.dst.asset}/sprite.svg`.replace(dir.dst._, dir.src._),
      `${dir.dst.asset}/symbol-defs.svg`.replace(dir.dst._, dir.src._),
      `${dir.dst.asset}/images/slogan1.jpg`.replace(dir.dst._, dir.src._),
      `${dir.dst.asset}/images/slogan2.jpg`.replace(dir.dst._, dir.src._),
      `${dir.dst.asset}/images/slogan3.jpg`.replace(dir.dst._, dir.src._),
      `${dir.dst.asset}/images/smoke3.jpg`.replace(dir.dst._, dir.src._),
    ],
    app: [
      `${dir.src.js}/app.js`,
      `${dir.src.sass._}/app.scss`,
    ],
    m: `${dir.src.sass._}/zaza-ui/m.scss`,
    p: `${dir.src.sass._}/zaza-ui/p.scss`,
  },
  context: path.resolve(__dirname, `.${dir._}`),
  output: {
    filename: `${dir.dst.js}/[name]${production ? '-[chunkhash]' : ''}.js`.replace(`${dir.dst._}/`, ''),
    path: path.resolve(__dirname, (production ? `.${dir.dst._}` : `.${dir.tmp}`)),
    publicPath: '/'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: style.css
      },
      {
        test: /\.scss$/,
        use: style.scss
      },
      {
        test: /\.vue$/,
        use: [
          {
            loader: 'vue-loader',
            options: {
              loaders: {
                css: style.css,
                scss: style.scss,
                i18n: '@kazupon/vue-i18n-loader',
              }
            }
          }
        ]
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader'
          }
        ]
      },
      {
        test: /\.(png|svg|jpe?g|gif)/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: `[name].[ext]`,
              outputPath: `${dir.dst.asset}/`.replace(`${dir.dst._}/`, '')
            }
          }
        ]
      }
    ]
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    },
    extensions: ['*', '.js', '.vue', '.json']
  },
  performance: {
    hints: false
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: `${dir.src._}/index.html`,
      filename: path.resolve(__dirname, `.${production ? dir.dst._ : dir.tmp }/index.html`)
    }),
    new Dotenv(),
    new WebpackRTLPlugin()
  ]
};

module.exports = common;
