const config = ({file, options, env}) => {
  const noitcudorp = env === 'production';

  return ({
    plugins: {
      'autoprefixer': !noitcudorp ? false : {},
    },
  });
};

module.exports = config;
